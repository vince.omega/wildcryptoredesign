<div class="account">
    <div class="account__section">
        <div class="account__headerbox">
            <h2 class="account__header">{$Top.i18n("AccountPage__Games__TitleMessage")}</h2>
        </div>

        <div class="account__content">
            <table class="data-table responsive" data-url="{$Link(iwg)}">
                <thead>
                <tr>
                    <th>{$Top.i18n("AccountPage__Games__Table__Purchased")}</th>
                    <th>{$Top.i18n("AccountPage__Games__Table__Completed")}</th>
                    <th>{$Top.i18n("AccountPage__Games__Table__Name")}</th>
                    <th>{$Top.i18n("AccountPage__Games__Table__Winner")}</th>
                    <th>{$Top.i18n("AccountPage__Games__Table__Winnings")}</th>
                    <th>{$Top.i18n("AccountPage__Games__Table__AutoCompleted")}</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>