<div class="cmsinput cmsinput--faint cmsinput--links">
    $Content

    <div class="containerwrap containerwrap--expand containerwrap--justifycenter containerwrap--center">
        <a class="button button--bloodred containerwrap__item containerwrap__item--gap" href="/logout">{$Top.i18n("Terms__Decline")}</a>
        <a class="button button--sushi containerwrap__item containerwrap__item--gap" href="$Link(ConfirmTerms)">{$Top.i18n("Terms__Accept")}</a>
    </div>
</div>